from common import *

def get_raw(h5_rawfile = '/home/calder/Work/factor_model/data/data_for_matt.hdf'):
    return pd.read_hdf(h5_rawfile, 'table')

def get_df(date, lookback, loc, h5_filename = "factor.h5"):
    """
    Extract data frame from h5 file in (date, lookback)
    :param date: Date string in 'yyyy-mm-dd' format.
    :param lookback: Lookback.
    :param loc: Location of data in file.
    :param h5_filename: h5 data source.
    :return: Dataframe
    """
    df = pd.read_hdf(h5_filename, loc)
    return df[df.index <= date].tail(lookback)

def get_adjret(date, lookback, h5_filename = "factor.h5"):
    """
    Get adjusted return for names in (date, lookback).
    :param date: Date string in 'yyyy-mm-dd' format.
    :param lookback: Lookback.
    :param h5_filename: h5 data source.
    :return: Matrix of adjusted returns.
    """
    return get_df(date, lookback, 'adjret', h5_filename)
          
def get_stddev(date, lookback, h5_filename = "factor.h5"):
    """
    Get return volatility for names in (date, lookback).
    :param date: Date string in 'yyyy-mm-dd' format.
    :param lookback: Lookback.
    :param h5_filename: h5 data source.
    :return: Matrix of volatilities.
    """
    return get_df(date, lookback, 'stddev', h5_filename)

def get_momentum(date, lookback, term = "short", h5_filename = "factor.h5"):
    """
    Get the momentum (ma(w1) - ma(w0)) for names in (date, lookback).
    :param date: Date string in 'yyyy-mm-dd' format.
    :param lookback: Lookback.
    :param term: Time horizon ('long' or 'short').
    :param h5_filename: h5 data source.
    :return: Matrix of momentum score.
    """
    return get_df(date, lookback, 'momentum_{t}'.format(t = term), h5_filename)

def get_ep_ratio(date, lookback, h5_filename = "factor.h5"):
    """
    Get earnings to price for names in (date, lookback).
    :param date: Date string in 'yyyy-mm-dd' format.
    :param lookback: Lookback.
    :param h5_filename: h5 data source.
    :return: Matrix of ep_ratio.
    """
    return get_df(date, lookback, 'ep_ratio', h5_filename)

def get_bp_ratio(date, lookback, h5_filename = "factor.h5"):
    """
    Get book to price for names in (date, lookback).
    :param date: Date string in 'yyyy-mm-dd' format.
    :param lookback: Lookback.
    :param h5_filename: h5 data source.
    :return: Matrix of bp_ratio.
    """
    return get_df(date, lookback, 'bp_ratio', h5_filename)

def get_sp_ratio(date, lookback, h5_filename = "factor.h5"):
    """
    Get sales to price for names in (date, lookback).
    :param date: Date string in 'yyyy-mm-dd' format.
    :param lookback: Lookback.
    :param h5_filename: h5 data lookbackource.
    :return: Matrix of ep_ratio.
    """
    return get_df(date, lookback, 'sp_ratio', h5_filename)

def get_mktcap(date, lookback, h5_filename = "factor.h5"):
    """
    Get the marketcap for names in (date, lookback).
    :param date: Date string in 'yyyy-mm-dd' format.
    :param lookback: Lookback.
    :param h5_filename: h5 data source.
    :return: Matrix of marketcap.
    """
    return get_df(date, lookback, 'mktcap', h5_filename)
        
def get_dvolume(date, lookback, h5_filename = "factor.h5"):
    """
    Get the dollar volume for names in (date, lookback).
    :param date: Date string in 'yyyy-mm-dd' format.
    :param lookback: Lookback.
    :param h5_filename: h5 data source.
    :return: Matrix of dollar volume.
    """
    return get_df(date, lookback, 'dvolume', h5_filename)
        
def get_dividend(date, lookback, h5_filename = "factor.h5"):
    """
    Get the dollar volume for names in (date, lookback).
    :param date: Date string in 'yyyy-mm-dd' format.
    :param lookback: Lookback.
    :param h5_filename: h5 data source.
    :return: Matrix of dollar volume.
    """
    return get_df(date, lookback, 'dividend', h5_filename)

def get_portret(date, lookback, h5_filename  = "factor.h5"):
    """
    Get the portfolio return data in (date, lookback).
    :param date: Date string in 'yyyy-mm-dd' format.
    :param lookback: Lookback.
    :param h5_filename: h5 data source.
    :return: Matrix of portfolio returns.
    """
    return get_df(date, lookback, 'portret', h5_filename)

def get_portpos(date, lookback, h5_filename  = "factor.h5"):
    """
    Get the portfolio weights data in (date, lookback).
    :param date: Date string in 'yyyy-mm-dd' format.
    :param lookback: Lookback.
    :param h5_filename: h5 data source.
    :return: Matrix of portfolio positions.
    """
    return get_df(date, lookback, 'portpos', h5_filename)

def get_indices(date, lookback, indices = "mktret", h5_filename = "factor.h5"):
    """
    Get the factor returns for the specified factors.
    :param date: Date string in 'yyyy-mm-dd' format.
    :param lookback: Lookback.
    :indices: Which index set to retrieve (eg. mktret, secret, indret).
    """
    return get_df(date, lookback, indices, h5_filename)        
        
def get_factors(date, lookback, factors = "pfactors", h5_filename = "factor.h5"):
    """
    Get the factor returns for the specified factors.
    :param date: Date string in 'yyyy-mm-dd' format.
    :param lookback: Lookback.
    :factors: Which factor set to retrieve (eg. pfactors, sfactors, ifactors).
    """
    return get_df(date, lookback, factors, h5_filename)

def get_zscores(date, h5_filename = "factor.h5"):
    """
    Get the z-scores for the given date across the factors.
    """
    zscores = []
    zlabels = ['zliq', 'zbet', 'zvol', 'zxmo', 'zsiz', 'zval', 'zepr', 'zgro']
    for z in zlabels:    
        zscores.append(get_df(date, 1, z, h5_filename))
    df_zscores = pd.concat(zscores, axis=0)
    df_zscores.index = zlabels
    return df_zscores.T

def get_sectors(date, h5_filename = "factor.h5"):
    """
    Get the sector indicators for the given date.
    """
    df_sectors = pd.get_dummies(get_df(date, 1, 'sector', h5_filename).iloc[0])
    df_sectors['Market'] = 1
    return df_sectors

def get_industries(date, h5_filename = "factor.h5"):
    """
    Get the industry indicators for the given date.
    """
    df_industries = pd.get_dummies(get_df(date, 1, 'industry', h5_filename).iloc[0])
    df_industries['Market'] = 1
    return df_industries   

def get_zsectors(date, h5_filename = "factor.h5"):
    """
    Get the zscores and sector indicators in one matrix.
    """
    return pd.concat([get_zscores(date, h5_filename), get_sectors(date, h5_filename)], axis = 1)

def get_zindustries(date, h5_filename = "factor.h5"):
    """
    Get the zscores and industry indicators in one matrix.
    """
    return pd.concat([get_zscores(date, h5_filename), get_industries(date, h5_filename)], axis = 1)
