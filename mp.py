import multiprocessing
import writers

if __name__ == '__main__':

    jobs = [
	    multiprocessing.Process(target=writers.write_adjret),
            multiprocessing.Process(target=writers.write_ep_ratio),
            multiprocessing.Process(target=writers.write_bp_ratio),
            multiprocessing.Process(target=writers.write_sp_ratio),
            multiprocessing.Process(target=writers.write_mktcap),
            multiprocessing.Process(target=writers.write_dvolume),
	    multiprocessing.Process(target=writers.write_dividend),
            multiprocessing.Process(target=writers.write_sector),
            multiprocessing.Process(target=writers.write_industry)
            ]

    jobs[0].start()
    jobs[1].start()
    jobs[1].join()

    jobs[2].start()
    jobs[3].start()
    jobs[3].join()

    jobs[4].start()
    jobs[5].start()
    jobs[5].join()

    jobs[6].start()
    jobs[7].start()
    jobs[7].join()

    jobs[8].start()
