from common import *
from get import *

def factor_beta(date, 
                lookback, 
                factors = 'pfactors', 
                factor_order = None, 
                h5_filename = 'factor.h5'):
    """
    Calculate factor loadings using the beta method, with regressions performed sequentially.
    :param date: Date string in 'yyyy-mm-dd' format.
    :param lookback: Lookback.
    :param factors: Factor set to use.
    :param factor_order: List of factors to fit and order to fit them (None for all).
    :param h5_filename: h5 data source.
    """
    if factors in ['pfactors', 'sfactors', 'ifactors']:
        df_adjret = mean_fillna(get_adjret(date, lookback, h5_filename = h5_filename))
        df_factors = get_factors(date, lookback, factors, h5_filename = h5_filename)
        df_factors['alpha'] = 1.0
        beta_list = []
        df_resid = df_adjret.copy()
        if factor_order is None:
            factor_order = [[f for f in df_factors.columns]]
        for f in factor_order:
            if not isinstance(f,list):
                f = [f]
            lm = LinearRegression(fit_intercept = False).fit(df_factors[f], df_resid)
            beta_list.append(pd.DataFrame(lm.coef_, index = df_adjret.columns, columns = f))
            df_resid = df_resid - pd.DataFrame(lm.predict(df_factors[f]), 
                                               index = df_adjret.index, 
                                               columns = df_adjret.columns)
        df_beta = pd.concat(beta_list, axis = 1) 
    elif factors == 'zfactors':
        df_beta = get_zscores(date, h5_filename)
    elif factors == 'zsfactors':
        df_beta = get_zsectors(date, h5_filename)
    elif factors == 'zifactors':
        df_beta = get_zindustries(date, h5_filename)
    df_beta.fillna(0, inplace = True)
    return df_beta

def ex_post_factor_exposure(date, 
                            lookback, 
                            factors = 'pfactors', 
                            factor_order = None, 
                            h5_filename = 'factor.h5'):
    """
    Calculate end-of-day ex-post factor exposures usnig beta method.
    :param date: Date string in 'yyyy-mm-dd' format.
    :param lookback: Lookback.
    :param factors: Factor set to use.
    :param factor_order: List of factors to fit and order to fit them (None for all).
    :param h5_filename: h5 data source.
    """
    df_factors = get_factors(date, lookback, factors, h5_filename)
    df_factors['alpha'] = 1.0
    df_portret = get_portret(date, lookback, h5_filename)
    df_portret.reindex(df_factors.index)
    df_portret.fillna(0, inplace = True)
    beta_list = []
    df_resid = df_portret.copy()
    ddate = datetime.strptime(date, '%Y-%m-%d')
    if factor_order is None:
        factor_order = [[f for f in df_factors.columns]]    
    for f in factor_order:
        if not isinstance(f,list):
            f = [f]
        lm = LinearRegression(fit_intercept = False).fit(df_factors[f], df_resid)
        beta_list.append(pd.DataFrame(lm.coef_, index = [ddate], columns = f))
        df_resid = df_resid - pd.DataFrame(lm.predict(df_factors[f]), 
                                           index = df_portret.index, 
                                           columns = df_portret.columns)
    return pd.concat(beta_list, axis = 1)                                     


def ex_post_factor_attribution(date, 
                               lookback,
                               factors = 'pfactors',
                               factor_order = None, 
                               h5_filename = 'factor.h5'):
    """
    Calculate ex-post factor attribution.
    :param date: Date string in 'yyyy-mm-dd' format.
    :param lookback: Lookback.
    :param factors: Factor set to use.
    :param factor_order: List of factors to fit and order to fit them (None for all).
    :param h5_filename: h5 data source.
    """
    df_exbeta = ex_post_factor_exposure(shift_date(date,-1), lookback, factors, factor_order, h5_filename)
    df_factors = get_factors(date, 1, factors, h5_filename)
    com = [c for c in df_factors.columns if (c in df_exbeta.columns and c in df_factors.columns)]
    return df_factors[com] * df_exbeta[com].values

def ex_ante_factor_exposure(date, 
                            lookback, 
                            factors = 'pfactors',
                            factor_order = None, 
                            h5_filename = 'factor.h5'):
    """
    Calculate end-of-day factor exposures using the beta method.
    :param date: Date string in 'yyyy-mm-dd' format.
    :param lookback: Lookback.
    :param factors: Factor set to use.
    :param factor_order: List of factors to fit and order to fit them (None for all).
    :param h5_filename: h5 data source.
    """
    df_portpos = get_portpos(date, 1, h5_filename)    
    if df_portpos.shape[0] == 0:
        df_portpos  = pd.DataFrame(0, index = [datetime.strptime(date, '%Y-%m-%d')], columns = df_portpos.columns)
    
    df_beta = factor_beta(date, lookback, factors, factor_order, h5_filename)
    
    com = np.intersect1d(df_portpos.columns, df_beta.index)
    return df_portpos[com].dot(df_beta.reindex(com))

def ex_ante_factor_attribution(date,
                               lookback, 
                               factors = 'pfactors', 
                               factor_order = None, 
                               h5_filename = 'factor.h5'):
    """
    Calculate factor exposures using the beta method.
    :param date: Date string in 'yyyy-mm-dd' format.
    :param lookback: Lookback.
    :param factors: Factor set to use.
    :param factor_order: List of factors to fit and order to fit them (None for all).
    :param h5_filename: h5 data source.
    """
    df_portexp = ex_ante_factor_exposure(shift_date(date,-1), lookback, factors, factor_order, h5_filename)
    df_pfactors = get_factors(date, 1, factors, h5_filename)
    com = np.intersect1d(df_portexp.columns, df_pfactors.columns)
    return df_pfactors[com] * df_portexp[com].values



if __name__ == "__main__":
    # Ex-post Feb18
    ex_ante_factor_attribution('2017-08-18', 125, 'pfactors')    