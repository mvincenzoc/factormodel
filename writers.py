from datetime import datetime, timedelta
import h5py as h5
import numpy as np
import pandas as pd


def write_adjret(winsorize = (0.5, 0.0), h5_filename = "factor_adjret.h5"):
    """
    Calculate adjusted returns and store to h5.
    :param df_raw: Dataframe of raw data.
    :param h5_filename: File to store adjusted returns to.
    """  
    df_raw = pd.read_hdf('/home/calder/Work/factor_model/data/data_for_matt.hdf', 'table')
    dates = sorted(np.unique(df_raw.data_date))
    names = sorted(np.unique(df_raw.ticker))
    df_adjret = pd.DataFrame(0, columns = names, index = dates)
    for e in names:
        # Extract adj-close for this name
        df_e = df_raw[df_raw.ticker == e]
        df_r = pd.DataFrame(df_e.adj_close.values, index = df_e.data_date).reindex(dates)
        # Replace any leading / lagging zero returns with nan
        nz = np.where(df_r != 0)
        nz_min = np.min(nz)
        if nz_min > 0:
            df_r.iloc[0:(nz_min-1)] = float('nan')
        nz_max = np.max(nz)
        if nz_max < len(df_r)-1:
            df_r.iloc[(nz_max+1):] = float('nan')
        # Assign this names close back to df
        df_adjret[e] = df_r.values
    df_adjret = (df_adjret.diff(1) / df_adjret.shift(1)).iloc[1:]
    if winsorize is not None:
        df_adjret[df_adjret > winsorize[0]] = winsorize[1]
        df_adjret[df_adjret < -winsorize[0]] = -winsorize[1]
    # Store result to h5
    df_adjret.to_hdf(h5_filename, 'adjret')
    return df_adjret


def write_ep_ratio(eps = 1e-6, h5_filename = "factor_ep_ratio.h5"):
    """
    Calculate the earnings to price ratio and store to h5.
    :param df_raw: Dataframe of raw data.
    :param eps: Minimum PE ratio to invert.
    :param h5_filename: File to store adjusted returns to.
    :return: Matrix of ep_ratios.
    """
    df_raw = pd.read_hdf('/home/calder/Work/factor_model/data/data_for_matt.hdf', 'table')
    dates = sorted(np.unique(df_raw.data_date))
    names = sorted(np.unique(df_raw.ticker))
    df_ep_ratio = pd.DataFrame(0, columns = names, index = dates)
    for e in names:
        df_e = df_raw[df_raw.ticker == e]
        df_r = pd.DataFrame(df_e.price_to_earning.values, index = df_e.data_date).reindex(dates)
        df_ep_ratio[e] = np.where(df_r > eps, 1.0 / df_r, 0)
    # Store result to h5
    df_ep_ratio.to_hdf(h5_filename, 'ep_ratio')
    return df_ep_ratio

def write_bp_ratio(h5_filename = "factor_bp_ratio.h5"):
    """
    Calculate the book to price ratio and store to h5.
    :param df_raw: Dataframe of raw data.
    :param h5_filename: File to store adjusted returns to.
    :return: Matrix of bp_ratios.
    """
    df_raw = pd.read_hdf('/home/calder/Work/factor_model/data/data_for_matt.hdf', 'table')
    dates = sorted(np.unique(df_raw.data_date))
    names = sorted(np.unique(df_raw.ticker))
    df_bp_ratio = pd.DataFrame(0, columns = names, index = dates)
    for e in names:
        df_e = df_raw[df_raw.ticker == e]
        df_bp_ratio[e] = pd.DataFrame(df_e.book_market_value.values, index = df_e.data_date).reindex(dates)
    # Store result to h5
    df_bp_ratio.to_hdf(h5_filename, 'bp_ratio')
    return df_bp_ratio

def write_sp_ratio(eps = 1e-6, h5_filename = "factor_sp_ratio.h5"):
    """
    Calculate the sales to price ratio and store to h5.
    :param df_raw: Dataframe of raw data.
    :param eps: Minimum PS ratio to invert.
    :param h5_filename: File to store adjusted returns to.
    :return: Matrix of sp_ratios.
    """
    df_raw = pd.read_hdf('/home/calder/Work/factor_model/data/data_for_matt.hdf', 'table')
    dates = sorted(np.unique(df_raw.data_date))
    names = sorted(np.unique(df_raw.ticker))
    df_sp_ratio = pd.DataFrame(0, columns = names, index = dates)
    for e in names:
        df_e = df_raw[df_raw.ticker == e]
        df_r = pd.DataFrame(df_e.price_to_sales.values, index = df_e.data_date).reindex(dates)
        df_sp_ratio[e] = np.where(df_r > eps, 1.0 / df_r, 0)
    # Store result to h5
    df_sp_ratio.to_hdf(h5_filename, 'sp_ratio')
    return df_sp_ratio

def write_mktcap(h5_filename = "factor_mktcap.h5"):
    """
    Extract marketcap and store to h5.
    :param df_raw: Dataframe of raw data.
    :param h5_filename: File to store adjusted returns to.
    :return: Matrix of marketcap.
    """
    df_raw = pd.read_hdf('/home/calder/Work/factor_model/data/data_for_matt.hdf', 'table')
    dates = sorted(np.unique(df_raw.data_date))
    names = sorted(np.unique(df_raw.ticker))
    df_mktcap = pd.DataFrame(0, columns = names, index = dates)
    for e in names:
        df_e = df_raw[df_raw.ticker == e]
        df_mktcap[e] = pd.DataFrame(df_e.market_cap.values, index = df_e.data_date).reindex(dates)
    # Store result to h5
    df_mktcap.to_hdf(h5_filename, 'mktcap')
    return df_mktcap

def write_dvolume(h5_filename = "factor_dvolume.h5"):
    """
    Extract dollar volume and store to h5.
    :param df_raw: Dataframe of raw data.
    :param h5_filename: File to store adjusted returns to.
    :return: Matrix of dollar volume.
    """
    df_raw = pd.read_hdf('/home/calder/Work/factor_model/data/data_for_matt.hdf', 'table')
    dates = sorted(np.unique(df_raw.data_date))
    names = sorted(np.unique(df_raw.ticker))
    df_dvolume = pd.DataFrame(0, columns = names, index = dates)
    for e in names:
        df_e = df_raw[df_raw.ticker == e]
        df_r = pd.DataFrame(df_e.adj_vol.values * df_e.adj_close.values, index = df_e.data_date).reindex(dates)
        df_dvolume[e] = df_r.values
    # Store result to h5
    df_dvolume.to_hdf(h5_filename, 'dvolume')
    return df_dvolume

def write_dividend(h5_filename = "factor_dividend.h5"):
    """
    Extract dividend yield and store to h5.
    :param df_raw: Dataframe of raw data.
    :param h5_filename: File to store adjusted returns to.
    :return: Matrix ofdividend yield.
    """
    df_raw = pd.read_hdf('/home/calder/Work/factor_model/data/data_for_matt.hdf', 'table')
    dates = sorted(np.unique(df_raw.data_date))
    names = sorted(np.unique(df_raw.ticker))
    df_dividend = pd.DataFrame(0, columns = names, index = dates)
    for e in names:
        df_e = df_raw[df_raw.ticker == e]
        df_r = pd.DataFrame(df_e.dividend_payout_ratio.values, index = df_e.data_date).reindex(dates)
        df_dividend[e] = df_r.values
    # Store result to h5
    df_dividend.to_hdf(h5_filename, 'dividend')
    return df_dividend

def write_sector(h5_filename = "factor_sector.h5"):
    """
    Get the sector and store to h5.
    :param df_h5: Raw data.
    :param h5_filename: File to store adjusted returns to.
    :return: Matrix of sectors.
    """
    df_raw = pd.read_hdf('/home/calder/Work/factor_model/data/data_for_matt.hdf', 'table')
    dates = sorted(np.unique(df_raw.data_date))
    names = sorted(np.unique(df_raw.ticker))
    df_sector = pd.DataFrame(0, columns = names, index = dates)
    for e in names:
        df_e = df_raw[df_raw.ticker == e]
        df_s = pd.DataFrame(df_e.zacks_x_sector_desc.values, index = df_e.data_date).reindex(dates)
        df_sector[e] = df_s.values
    # Store result to h5
    df_sector.to_hdf(h5_filename, 'sector')
    return df_sector

def write_industry(h5_filename = "factor_industry.h5"):
    """
    Get the industry and store to h5.
    :param df_h5: Raw data.
    :param h5_filename: File to store adjusted returns to.
    :return: Matrix of industries.
    """
    df_raw = pd.read_hdf('/home/calder/Work/factor_model/data/data_for_matt.hdf', 'table')
    dates = sorted(np.unique(df_raw.data_date))
    names = sorted(np.unique(df_raw.ticker))
    df_industry = pd.DataFrame(0, columns = names, index = dates)
    for e in names:
        df_e = df_raw[df_raw.ticker == e]
        df_i = pd.DataFrame(df_e.zacks_m_ind_desc.values, index = df_e.data_date).reindex(dates)
        df_industry[e] = df_i.values
    # Store result to h5
    df_industry.to_hdf(h5_filename, 'industry')
    return df_industry


