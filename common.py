from datetime import datetime, timedelta
import h5py as h5
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.linear_model import LinearRegression
from scipy.stats import zscore

import quadprog

def shift_date(date, shift = -1):
    """
    Shift the yyyy-mm-dd date by shift days.
    :param date: yyyy-mm-dd date.
    :param shift: Number of days to shift date by.
    """
    return datetime.strftime(datetime.strptime(date, '%Y-%m-%d') + timedelta(days = shift), "%Y-%m-%d")
    
def mean_fillna(df):
    """
    Fill missing values of df with the row mean
    """
    df = df.copy()
    df_mu = np.nanmean(df.values, axis = 1)
    for c in df.columns:
        df[c] = np.where(df[c].isna(), df_mu, df[c])
    return df
    
def standardize(df, win, scale = 0.01):
    """
    Divide the columns of df by their rolling standard deviation.
    :param df: Data to standardize.
    :param win: Rolling window to use.
    :param scale: Target this standard deviation.
    :return: Standardized values of df.
    """
    return scale * df / df.rolling(win).std()
    
def zscore(df):
    """
    Helper for apply(zscore) operation.
    :param df: Dataframe of a row of a factor score.
    """
    return (df - df.mean()) / df.std()
    
def percentile_factor(df_adjret, df_score, label = '', percentile = 10, one_sided = False, std_win = 100):
    """
    Get the average return of values > upper percentile minus average of values < lower percentile.
    :param df_adjret: Dataframe of returns to average.
    :param df_score: Dataframe of scores to base percentiles on.
    :param percentile: Cutoff level.
    :param one_sided: Only average upper percentile.
    :param std_win: If > 0, standardize the factor.
    :return: Average return
    """
    # Use end of day score for next day returns
    df_score = df_score.shift(1)
    # Align score to adjret
    df_score = df_score.reindex(df_adjret.index)
    df_score = df_score[df_adjret.columns]
    # Calculate upper percentile and lower percentile if using
    p_hi = np.nanpercentile(df_score, 100-percentile, axis = 1).reshape((df_score.shape[0],1))
    if not one_sided:
        p_lo = np.nanpercentile(df_score, percentile, axis = 1).reshape((df_score.shape[0],1))
    # Calculate average returns
    f_ret = np.nanmean(np.where(df_score.values >= p_hi, df_adjret, np.nan), axis = 1)
    if not one_sided:
        f_ret = f_ret - np.nanmean(np.where(df_score.values <= p_lo, df_adjret, np.nan), axis = 1)
    f_ret = pd.DataFrame({label: f_ret}, index = df_adjret.index)
    # Standardize
    if std_win > 0:
        f_ret = standardize(f_ret, std_win)
    return f_ret

def inventory(h5_filename = 'factor.h5'):
    """
    Print data frames and shapes of the top level objects in the h5 file.
    :param h5_filename: The h5 file.
    """
    fh5 = h5.File(h5_filename)
    for k in fh5.keys():
        print((k, pd.read_hdf(h5_filename, k).shape))
    fh5.close()
    
def survey(date, lookback, factors = 'pfactors', h5_filename = "factor.h5"):
    """
    Plot the factors.
    :param date: Date string in 'yyyy-mm-dd' format.
    :param lookback: Lookback.
    :param factors: Which factors to plot.
    :param h5_filename: h5 data source.
    """
    df_factors = get_pfactors(date, lookback)
    for c in df_factors.columns:
        df_factors[c].cumsum().plot(title = c)
        plt.show()
    plt.close()    
