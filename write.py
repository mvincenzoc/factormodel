from common import *
from get import *

def write_all(h5_rawfile = "/home/calder/Work/factor_model/data/data_for_matt_03_25_18.hdf",
              h5_filename = "factor.h5"):
    """
    Extract data from raw file into common format and write to h5.
    :param h5_rawfile: Source.
    :param h5_filename: Sink.
    """
    df_raw = get_raw(h5_rawfile)
    
    write_adjclose(df_raw, h5_filename)
    
    df_adjret = write_adjret(df_raw, (0.5,0.0), h5_filename)

    write_stddev(df_adjret, 25)
    write_momentum(df_adjret, 5, 25, "short")
    write_momentum(df_adjret, 25, 250, "long")

    write_ep_ratio(df_raw, 1e-6, h5_filename)
    write_sp_ratio(df_raw, 1e-6, h5_filename)
    write_bp_ratio(df_raw, 1e-6, h5_filename)

    write_mktcap(df_raw, h5_filename)
    write_dvolume(df_raw, h5_filename)
    write_dividend(df_raw, h5_filename)

    write_sector(df_raw, h5_filename)
    write_industry(df_raw, h5_filename)
    
    write_market_index(100, 3e8, h5_filename)
    write_sector_indices(100, 3e8, h5_filename)
    write_industry_indices(100, 3e8, h5_filename)
    
    write_pfactors(100, 3e8, h5_filename)
    write_sfactors(h5_filename)
    write_ifactors(h5_filename)


def write_adjclose(df_raw, h5_filename = "factor.h5"):
    """
    Calculate adjusted returns and store to h5.
    :param df_raw: Dataframe of raw data.
    :param winsorize: None or tuple of (tol, replace). 
    :param h5_filename: File to store adjusted returns to.
    """
    df_adjclose = df_raw.set_index(['data_date', 'ticker'])['adj_close'].unstack(level = 'ticker')
    # Store result to h5
    df_adjclose.to_hdf(h5_filename, 'adjclose')
    return df_adjclose

def write_adjret(df_raw, winsorize = (0.5, 0.0), h5_filename = "factor.h5"):
    """
    Calculate adjusted returns and store to h5.
    :param df_raw: Dataframe of raw data.
    :param winsorize: None or tuple of (tol, replace). 
    :param h5_filename: File to store adjusted returns to.
    """  
    dates = sorted(np.unique(df_raw.data_date))
    names = sorted(np.unique(df_raw.ticker))
    df_adjret = pd.DataFrame(0, columns = names, index = dates)
    for e in names:
        # Extract adj-close for this name
        df_e = df_raw[df_raw.ticker == e]
        df_r = pd.DataFrame(df_e.adj_close.values, index = df_e.data_date).reindex(dates)
        # Replace any leading / lagging zero returns with nan
        nz = np.where(df_r != 0)
        nz_min = np.min(nz)
        if nz_min > 0:
            df_r.iloc[0:(nz_min-1)] = float('nan')
        nz_max = np.max(nz)
        if nz_max < len(df_r)-1:
            df_r.iloc[(nz_max+1):] = float('nan')
        # Assign this names close back to df
        df_adjret[e] = df_r.values
    df_adjret = (df_adjret.diff(1) / df_adjret.shift(1)).iloc[1:]
    if winsorize is not None:
        df_adjret[df_adjret > winsorize[0]] = winsorize[1]
        df_adjret[df_adjret < -winsorize[0]] = -winsorize[1]

    # Store result to h5
    df_adjret.to_hdf(h5_filename, 'adjret')
    return df_adjret

def write_stddev(df_adjret, win = 25, h5_filename = "factor.h5"):
    """
    Calculate rolling return volatility and store to h5.
    :param df_adjret: Dataframe of adjusted returns.
    :param win: Lookback window for calculating std dev.
    :param h5_filename: File to store adjusted returns to.
    :return: Matrix of volatilities.
    """
    df_stddev = df_adjret.rolling(win).std()
    # Store result to h5
    df_stddev.to_hdf(h5_filename, 'stddev')
    return df_stddev

def write_momentum(df_adjret, win0 = 5, win1 = 25, term = "short", h5_filename = "factor.h5"):
    """
    Calculate the momentum (ma(win1) - ma(win0)) and store to h5.
    :param df_adjret: Dataframe of adjusted returns.
    :param win0: Small lookback window.
    :param win1: Large lookback window.
    :param term: Label for momentum (eg. "short" or "long")
    :param h5_filename: File to store adjusted returns to.
    :return: Matrix of momentum scores.
    """
    df_momentum = df_adjret.rolling(win1).mean() - df_adjret.rolling(win0).mean()
    # Store result to h5
    df_momentum.to_hdf(h5_filename, 'momentum_{t}'.format(t = term))
    return df_momentum

def write_axioma_momentum(h5_filename = "factor.h5"):
    """
    Calculate the momentum as cumulative return over year, exclusing last month and store to h5.
    :param h5_filename: File to store adjusted returns to.
    :return: Matrix of momentum scores.
    """
    df_adjret = pd.read_hdf(h5_filename, 'adjret')
    df_xmom = df_adjret.rolling(250).sum() - df_adjret.rolling(22).sum()
    # Store result to h5
    df_xmom.to_hdf(h5_filename, 'xmom')
    return df_xmom

#def write_ep_ratio(df_raw, eps = 1e-6, h5_filename = "factor.h5"):
    #"""
    #Calculate the earnings to price ratio and store to h5.
    #:param df_raw: Dataframe of raw data.
    #:param eps: Minimum PE ratio to invert.
    #:param h5_filename: File to store adjusted returns to.
    #:return: Matrix of ep_ratios.
    #"""
    #dates = sorted(np.unique(df_raw.data_date))
    #names = sorted(np.unique(df_raw.ticker))
    #df_ep_ratio = pd.DataFrame(0, columns = names, index = dates)
    #for e in names:
        #df_e = df_raw[df_raw.ticker == e]
        #df_r = pd.DataFrame(df_e.price_to_earning.values, index = df_e.data_date).reindex(dates)
        #df_ep_ratio[e] = np.where(df_r > eps, 1.0 / df_r, 0)
    ## Store result to h5
    #df_ep_ratio.to_hdf(h5_filename, 'ep_ratio')
    #return df_ep_ratio

def write_ep_ratio(df_raw, winsorize = (10, 10), h5_filename = "factor.h5"):
    """
    Calculate the earnings to price ratio and store to h5.
    :param df_raw: Dataframe of raw data.
    :param eps: Minimum PE ratio to invert.
    :param h5_filename: File to store adjusted returns to.
    :return: Matrix of ep_ratios.
    """
    df_ep_ratio = df_raw.set_index(['data_date', 'ticker']).price_to_earning.unstack()
    df_ep_ratio = pd.DataFrame(1.0 / df_ep_ratio, 
                               index = df_ep_ratio.index,
                               columns = df_ep_ratio.columns)
    if winsorize is not None:
        df_ep_ratio[df_ep_ratio > winsorize[0]] = winsorize[1]
        df_ep_ratio[df_ep_ratio < -winsorize[0]] = -winsorize[1]

    df_ep_ratio.to_hdf(h5_filename, 'ep_ratio')
    return df_ep_ratio

def write_bp_ratio(df_raw, winsorize = (10, 10), h5_filename = "factor.h5"):
    """
    Calculate the book to price ratio and store to h5.
    :param df_raw: Dataframe of raw data.
    :param h5_filename: File to store adjusted returns to.
    :return: Matrix of bp_ratios.
    """
    df_bp_ratio = df_raw.set_index(['data_date', 'ticker']).book_market_value.unstack()
    if winsorize is not None:
        df_bp_ratio[df_bp_ratio > winsorize[0]] = winsorize[1]
        df_bp_ratio[df_bp_ratio < -winsorize[0]] = -winsorize[1]
    
    df_bp_ratio.to_hdf(h5_filename, 'bp_ratio')
    return df_bp_ratio

def write_sp_ratio(df_raw, winsorize = (50, 50), h5_filename = "factor.h5"):
    """
    Calculate the sales to price ratio and store to h5.
    :param df_raw: Dataframe of raw data.
    :param eps: Minimum PS ratio to invert.
    :param h5_filename: File to store adjusted returns to.
    :return: Matrix of sp_ratios.
    """
    df_sp_ratio = df_raw.set_index(['data_date', 'ticker']).price_to_sales.unstack()
    df_sp_ratio = pd.DataFrame(1.0 / df_sp_ratio, 
                               index = df_sp_ratio.index,
                               columns = df_sp_ratio.columns)
    if winsorize is not None:
        df_sp_ratio[df_sp_ratio > winsorize[0]] = winsorize[1]
        df_sp_ratio[df_sp_ratio < -winsorize[0]] = -winsorize[1]

    df_sp_ratio.to_hdf(h5_filename, 'sp_ratio')
    return df_sp_ratio


def write_mktcap(df_raw, h5_filename = "factor.h5"):
    """
    Extract marketcap and store to h5.
    :param df_raw: Dataframe of raw data.
    :param h5_filename: File to store adjusted returns to.
    :return: Matrix of marketcap.
    """
    dates = sorted(np.unique(df_raw.data_date))
    names = sorted(np.unique(df_raw.ticker))
    df_mktcap = pd.DataFrame(0, columns = names, index = dates)
    for e in names:
        df_e = df_raw[df_raw.ticker == e]
        df_mktcap[e] = pd.DataFrame(df_e.mkt_cap.values, index = df_e.data_date).reindex(dates)
    # Store result to h5
    df_mktcap.to_hdf(h5_filename, 'mktcap')
    return df_mktcap

def write_dvolume(df_raw, h5_filename = "factor.h5"):
    """
    Extract dollar volume and store to h5.
    :param df_raw: Dataframe of raw data.
    :param h5_filename: File to store adjusted returns to.
    :return: Matrix of dollar volume.
    """
    dates = sorted(np.unique(df_raw.data_date))
    names = sorted(np.unique(df_raw.ticker))
    df_dvolume = pd.DataFrame(0, columns = names, index = dates)
    for e in names:
        df_e = df_raw[df_raw.ticker == e]
        df_r = pd.DataFrame(df_e.adj_vol.values * df_e.adj_close, index = df_e.data_date).reindex(dates)
        df_dvolume[e] = df_r.values
    # Store result to h5
    df_dvolume.to_hdf(h5_filename, 'dvolume')
    return df_dvolume

def write_dividend(df_raw, h5_filename = "factor.h5"):
    """
    Extract dividend yield and store to h5.
    :param df_raw: Dataframe of raw data.
    :param h5_filename: File to store adjusted returns to.
    :return: Matrix ofdividend yield.
    """
    dates = sorted(np.unique(df_raw.data_date))
    names = sorted(np.unique(df_raw.ticker))
    df_dividend = pd.DataFrame(0, columns = names, index = dates)
    for e in names:
        df_e = df_raw[df_raw.ticker == e]
        df_r = pd.DataFrame(df_e.dividend_payout_ratio, index = df_e.data_date).reindex(dates)
        df_dividend[e] = df_r.values
    # Store result to h5
    df_dividend.to_hdf(h5_filename, 'dividend')
    return df_dividend

def write_sector(df_raw, h5_filename = "factor.h5"):
    """
    Get the sector and store to h5.
    :param df_h5: Raw data.
    :param h5_filename: File to store adjusted returns to.
    :return: Matrix of sectors.
    """
    dates = sorted(np.unique(df_raw.data_date))
    names = sorted(np.unique(df_raw.ticker))
    df_sector = pd.DataFrame(0, columns = names, index = dates)
    for e in names:
        df_e = df_raw[df_raw.ticker == e]
        df_s = pd.DataFrame(df_e.zacks_x_sector_desc, index = df_e.data_date).reindex(dates)
        df_sector[e] = df_r.values
    # Store result to h5
    df_sector.to_hdf(h5_filename, 'sector')
    return df_sector

def write_industry(df_raw, h5_filename = "factor.h5"):
    """
    Get the industry and store to h5.
    :param df_h5: Raw data.
    :param h5_filename: File to store adjusted returns to.
    :return: Matrix of industries.
    """
    dates = sorted(np.unique(df_raw.data_date))
    names = sorted(np.unique(df_raw.ticker))
    df_industry = pd.DataFrame(0, columns = names, index = dates)
    for e in names:
        df_e = df_raw[df_raw.ticker == e]
        df_s = pd.DataFrame(df_e.zacks_m_ind_desc, index = df_e.data_date).reindex(dates)
        df_industry[e] = df_r.values
    # Store result to h5
    df_industry.to_hdf(h5_filename, 'industry')
    return df_industry


def write_market_index(std_win = 100, min_cap = 3e8, h5_filename = 'factor.h5'):
    """
    Write cap-weighted market index to h5 file.
    :param std_win: Window for standardization.
    :param min_cap: Minimum market cap to consider.
    :param h5_filename: File to store index to.
    """    
    df_adjret = pd.read_hdf(h5_filename, 'adjret')
    df_mktcap = pd.read_hdf(h5_filename, 'mktcap')
    df_adjret[df_mktcap < min_cap] = np.nan
    df_mktcap[df_mktcap < min_cap] = np.nan
    df_mktret = (df_adjret * df_mktcap).mean(axis = 1)
    df_mktret.rename(columns = ['Market'], inplace = True)
    df_mktret = standardize(df_mktret, std_win)
    df_mktret.fillna(0, inplace = True)
    df_mktret.to_hdf(h5_filename, 'mktret')
    return df_mktret

def write_sector_indices(std_win = 100, min_cap = 3e8, h5_filename = 'factor.h5'):
    """
    Write cap-weighted sector index returns to h5 file.
    :param std_win: Window for standardization.
    :param min_cap: Minimum market cap to consider.
    :param h5_filename: File to store indices to.
    """    
    df_sector = pd.read_hdf(h5_filename, 'sector')
    df_adjret = pd.read_hdf(h5_filename, 'adjret')
    df_mktcap = pd.read_hdf(h5_filename, 'mktcap')
    df_sector[df_mktcap < min_cap] = np.nan
    df_adjret[df_mktcap < min_cap] = np.nan
    df_mktcap[df_mktcap < min_cap] = np.nan
    df_sector[df_sector.isnull()] = ""
    sectors = np.unique(df_sector.values.flatten())
    sectors = [s for s in sectors if s]
    df_secret = pd.DataFrame(0, index = df_adjret.index, columns = sectors)
    for s in sectors:
        df_secret[s] = (df_adjret * np.where(df_sector == s, df_mktcap, 0)).mean(axis = 1)
    df_secret = standardize(df_secret, std_win)
    df_secret.fillna(0, inplace = True)
    df_secret.to_hdf(h5_filename, 'secret')
    return df_secret

def write_industry_indices(std_win = 100, min_cap = 3e8, h5_filename = 'factor.h5'):
    """
    Write cap-weighted industry index returns to h5 file.
    :param std_win: Window for standardization.
    :param min_cap: Minimum market cap to consider.
    :param h5_filename: File to store indices to.
    """
    df_industry = pd.read_hdf(h5_filename, 'sector')
    df_adjret = pd.read_hdf(h5_filename, 'adjret')
    df_mktcap = pd.read_hdf(h5_filename, 'mktcap')
    df_industry[df_mktcap < min_cap] = np.nan
    df_adjret[df_mktcap < min_cap] = np.nan
    df_mktcap[df_mktcap < min_cap] = np.nan
    df_industry[df_industry.isnull()] = ""
    industries = np.unique(df_industry.values.flatten())
    industries = [s for s in industries if s]
    df_indret = pd.DataFrame(0, index = df_adjret.index, columns = industries)
    for i in industries:
        df_indret[i] = (df_adjret * np.where(df_industry == i, df_mktcap, 0)).mean(axis = 1)
    df_indret = standardize(df_indret, std_win)
    df_indret.fillna(0, inplace = True)
    df_indret.to_hdf(h5_filename, 'indret')
    return df_indret

def write_pfactors(std_win = 100, min_cap = 3e8, h5_filename = 'factor.h5'):
    """
    Write percentile based factor indices to h5 file.
    :param std_win: Window for standardization.
    :param min_cap: Minimum market cap to consider.
    :param h5_filename: File to store factors to.
    """
    factors = {'smo': 'momentum_short',
               'lmo': 'momentum_long',
               'vol': 'stddev',
               'epr': 'ep_ratio',
               'spr': 'sp_ratio',
               'siz': 'mktcap',
               'liq': 'dvolume'
              }
    df_list = []
    df_adjret = pd.read_hdf(h5_filename, 'adjret')
    df_mktcap = pd.read_hdf(h5_filename, 'mktcap')
    df_adjret[df_mktcap < min_cap] = np.nan    
    for f in factors:
        df_s = pd.read_hdf(h5_filename, factors[f])
        df_s[df_mktcap < min_cap] = np.nan
        df_list.append(percentile_factor(df_adjret, df_s, f, std_win = std_win))
    df_factors = pd.concat(df_list, axis = 1)
    df_factors.fillna(0, inplace = True) # # # Look into where NaN's arise 
    df_factors.to_hdf(h5_filename, 'pfactors')
    return df_factors

def write_sfactors(min_cap = 3e8, h5_filename = 'factor.h5'):
    """
    Write percentile based factor indices to h5 file.
    :param min_cap: Minimum market cap.
    :param h5_filename: File to store factors to.
    """
    df_sector = pd.read_hdf(h5_filename, 'sector')
    df_adjret = pd.read_hdf(h5_filename, 'adjret')
    df_mktcap = pd.read_hdf(h5_filename, 'mktcap')
    df_sector[df_mktcap < min_cap] = np.nan
    df_adjret[df_mktcap < min_cap] = np.nan
    df_mktcap[df_mktcap < min_cap] = np.nan
    df_sector[df_sector.isnull()] = ""
    df_adjret.fillna(0.0, inplace = True)
    sectors = np.unique(df_sector.values.flatten())
    sectors = [s for s in sectors if s]
    sector_returns = []
    for t in range(1, df_adjret.shape[0]):
        df_secdum = pd.get_dummies(df_sector.iloc[t])
        df_secdum.rename(columns = {'':'Market'}, inplace = True)
        df_secdum['Market'] = 1
    
        P = np.dot(df_secdum.values.T, df_secdum.values).astype(float)
        q = np.dot(df_secdum.values.T, df_adjret.iloc[t].values)
        e = np.ones(P.shape[0]).astype(float)
        e[0] = 0.0
        r = quadprog.solve_qp(P, q, np.vstack(e), np.zeros(1, float), 1)[0]
        sector_returns.append(pd.DataFrame([r], index = [df_adjret.index[t]], columns = df_secdum.columns))
    df_factors = pd.concat(sector_returns)
    df_factors.fillna(0, inplace = True)
    df_factors.to_hdf(h5_filename, 'sfactors')
    return df_factors

def write_ifactors(min_cap = 3e8, h5_filename = 'factor.h5'):
    """
    Write percentile based factor indices to h5 file.
    :param min_cap: Minimum market cap.
    :param h5_filename: File to store factors to.
    """
    df_industry = pd.read_hdf(h5_filename, 'industry')
    df_adjret = pd.read_hdf(h5_filename, 'adjret')
    df_mktcap = pd.read_hdf(h5_filename, 'mktcap')
    df_industry[df_mktcap < min_cap] = np.nan
    df_adjret[df_mktcap < min_cap] = np.nan
    df_mktcap[df_mktcap < min_cap] = np.nan
    df_industry[df_industry.isnull()] = ""
    df_adjret.fillna(0.0, inplace = True)
    industries = np.unique(df_industry.values.flatten())
    industries = [i for i in industries if i]
    industry_returns = []
    for t in range(1, df_adjret.shape[0]):
        df_inddum = pd.get_dummies(df_industry.iloc[t])
        df_inddum.rename(columns = {'':'Market'}, inplace = True)
        df_inddum['Market'] = 1
    
        P = np.dot(df_inddum.values.T, df_inddum.values).astype(float)
        q = np.dot(df_inddum.values.T, df_adjret.iloc[t].values)
        e = np.ones(P.shape[0]).astype(float)
        e[0] = 0.0
        r = quadprog.solve_qp(P, q, np.vstack(e), np.zeros(1, float), 1)[0]
        industry_returns.append(pd.DataFrame([r], index = [df_adjret.index[t]], columns = df_inddum.columns))
    df_factors = pd.concat(industry_returns)
    df_factors.fillna(0, inplace = True)    
    df_factors.to_hdf(h5_filename, 'ifactors')
    return df_factors

def write_zliq(min_cap = 3e8, win = 75, h5_filename = 'factor.h5'):
    """
    Z-score of 3-month avg log dvolume / mktcap
    """
    df_mktcap = pd.read_hdf(h5_filename, 'mktcap')
    df_volume = pd.read_hdf(h5_filename, 'dvolume')
    
    df_volume = df_volume.rolling(win).mean()
    df_zliq = df_volume / df_mktcap
    # df_zliq = np.log(df_zliq)
    
    df_zliq[df_mktcap < min_cap] = np.nan
    df_zliq = df_zliq.apply(zscore, axis=1)
    df_zliq.to_hdf(h5_filename, 'zliq')
    return df_zliq

def write_zbet(min_cap = 3e8, win = 250, h5_filename = 'factor.h5'):
    """
    1 year daily beta
    """
    df_adjret = pd.read_hdf(h5_filename, 'adjret')
    df_adjret.fillna(0, inplace = True)
    df_mktret = pd.read_hdf(h5_filename, 'mktret')
    df_mktret.fillna(0, inplace = True)
    df_zbet = 0 * df_adjret
    for t in range(win, len(df_adjret)):
        lm = LinearRegression().fit(df_mktret.values[(t-win+1):(t+1)].reshape((win,1)), df_adjret.values[(t-win+1):(t+1),:])
        df_zbet.iloc[t,:] = np.squeeze(lm.coef_)

    df_mktcap = pd.read_hdf(h5_filename, 'mktcap')    
    df_zbet[df_mktcap < min_cap] = np.nan
    df_zbet = df_zbet.apply(zscore, axis=1)
    df_zbet.to_hdf(h5_filename, 'zbet')
    return df_zbet

def write_zvol(min_cap = 3e8, win = 125, h5_filename = 'factor.h5'):
    """
    6 month average of absolute returns over cross-sectional standard deviation,
    fully orthogonalized to Market Sensitivity (orthogonalized how?)
    """
    df_adjret = pd.read_hdf(h5_filename, 'adjret')
    df_zvol = df_adjret.abs().rolling(win).mean() 
    df_zvol = df_zvol.divide(df_adjret.std(axis = 1), axis = 0)
    df_mktcap = pd.read_hdf(h5_filename, 'mktcap')    
    df_zvol[df_mktcap < min_cap] = np.nan
    df_zvol = df_zvol.apply(zscore, axis=1)    
    df_zbet = pd.read_hdf(h5_filename, 'zbet')
    df_zvol = df_zvol - ((df_zvol * df_zbet).rolling(win).mean() / (df_zbet * df_zbet).rolling(win).mean()) * df_zbet
    df_zvol = df_zvol.apply(zscore, axis=1)
    return df_zvol

def write_zsmo(min_cap = 3e8, h5_filename = 'factor.h5'):
    """
    Short term momentum
    """
    df_zsmo = pd.read_hdf(h5_filename, 'momentum_short')
    df_mktcap = pd.read_hdf(h5_filename, 'mktcap')        
    df_zsmo[df_mktcap < min_cap] = np.nan
    df_zsmo = df_zsmo.apply(zscore, axis=1)
    df_zsmo.to_hdf(h5_filename, 'zsmo')
    return df_zsmo

def write_zlmo(min_cap = 3e8, h5_filename = 'factor.h5'):
    """
    Long term momentum
    """
    df_zlmo = pd.read_hdf(h5_filename, 'momentum_long')
    df_mktcap = pd.read_hdf(h5_filename, 'mktcap')        
    df_zlmo[df_mktcap < min_cap] = np.nan
    df_zlmo = df_zlmo.apply(zscore, axis=1)
    df_zlmo.to_hdf(h5_filename, 'zlmo')
    return df_zlmo

def write_zxmo(min_cap = 3e8, h5_filename = 'factor.h5'):
    """
    Mid term momentum, axioma style
    """
    df_zxmo = pd.read_hdf(h5_filename, 'xmom')
    df_mktcap = pd.read_hdf(h5_filename, 'mktcap')        
    df_zxmo[df_mktcap < min_cap] = np.nan
    df_zxmo = df_zxmo.apply(zscore, axis=1)
    df_zxmo.to_hdf(h5_filename, 'zxmo')
    return df_zxmo


def write_zsiz(min_cap = 3e8, h5_filename = 'factor.h5'):
    """
    Natural logarithm of market capitalization
    """
    df_mktcap = pd.read_hdf(h5_filename, 'mktcap')        
    df_zsiz = np.log(df_mktcap)
    df_zsiz[df_mktcap < min_cap] = np.nan
    df_zsiz = df_zsiz.apply(zscore, axis=1)
    df_zsiz.to_hdf(h5_filename, 'zsiz')
    return df_zsiz

def write_zval(min_cap = 3e8, h5_filename = 'factor.h5'):
    df_mktcap = pd.read_hdf(h5_filename, 'mktcap')    
    df_zval = pd.read_hdf(h5_filename, 'bp_ratio')
    df_zval[df_mktcap < min_cap] = np.nan
    df_zval = df_zval.apply(zscore, axis=1)
    df_zval.to_hdf(h5_filename, 'zval')
    return df_zval
    
def write_zepr(min_cap = 3e8, h5_filename = 'factor.h5'):
    df_mktcap = pd.read_hdf(h5_filename, 'mktcap')    
    df_zepr = pd.read_hdf(h5_filename, 'ep_ratio')
    df_zepr[df_mktcap < min_cap] = np.nan
    df_zepr = df_zepr.apply(zscore, axis=1)
    df_zepr.to_hdf(h5_filename, 'zepr')
    return df_zepr
    
def write_zgro(win = 75, min_cap = 3e8, h5_filename = 'factor.h5'):
    df_mktcap = pd.read_hdf(h5_filename, 'mktcap') 
    df_clo = pd.read_hdf(h5_filename, 'adjclose')
    df_epr = pd.read_hdf(h5_filename, 'ep_ratio') 
    df_spr = pd.read_hdf(h5_filename, 'sp_ratio')
    df_zgro = np.sqrt(df_epr * df_spr) * df_clo
    df_zgro = df_zgro.rolling(win).mean() - df_zgro.rolling(win).shift(win).mean()    
    df_zgro[df_mktcap < min_cap] = np.nan
    df_zgro = df_zgro.apply(zscore, axis=1)
    df_zgro.to_hdf(h5_filename, 'zgro')
    return df_zgro

def write_zscores(h5_filename = 'factor.h5'):
    write_zliq(h5_filename = h5_filename)
    write_zbet(h5_filename = h5_filename)
    write_zvol(h5_filename = h5_filename)
    write_zsmo(h5_filename = h5_filename)
    write_zlmo(h5_filename = h5_filename)
    write_zsiz(h5_filename = h5_filename)
    write_zval(h5_filename = h5_filename)
    write_zepr(h5_filename = h5_filename)
    write_zgro(h5_filename = h5_filename)
            

def write_zfactors(min_cap = 3e8, h5_filename = 'factor.h5'):
    df_adjret = pd.read_hdf(h5_filename, 'adjret')
    df_mktcap = pd.read_hdf(h5_filename, 'mktcap')
    df_adjret[df_mktcap < min_cap] = np.nan
    zfactors = []
    for t in range(len(df_adjret)):
        print(t)
        try:
            df_zscores = get_zscores(df_adjret.index[t].strftime("%Y-%m-%d"), h5_filename)
            df_zscores = df_zscores.dropna(axis = 1, how = 'all')
            df_zscores = df_zscores.dropna(axis = 0, how = 'any')
            df_ret = df_adjret.iloc[t,:][df_zscores.index].T
            df_zscores = df_zscores[~df_ret.isnull()]
            df_ret = df_ret[~df_ret.isnull()]
            df_ret = df_ret - df_ret.mean()
            df_wgt = df_mktcap.iloc[t,:][df_zscores.index].T
            
            lm = LinearRegression(fit_intercept = False).fit(df_zscores.values, df_ret.values, np.sqrt(df_wgt.values))
            zf = pd.DataFrame(lm.coef_, index = df_zscores.columns, columns = [df_adjret.index[t]]).T
            zfactors.append(zf)
        except Exception:
            print("Fail")    
    df_zfactors = pd.concat(zfactors)
    df_zfactors.fillna(0, inplace = True)
    df_zfactors.to_hdf(h5_filename, 'zfactors')
    return df_zfactors

def write_zsfactors(min_cap = 3e8, h5_filename = 'factor.h5'):
    df_adjret = pd.read_hdf(h5_filename, 'adjret')
    df_mktcap = pd.read_hdf(h5_filename, 'mktcap')
    df_sector = pd.read_hdf(h5_filename, 'sector')
    df_adjret[df_mktcap < min_cap] = np.nan
    df_sector[df_mktcap < min_cap] = np.nan
    df_sector[df_sector.isnull()] = ""
    zsfactors = []
    for t in range(len(df_adjret)):
        print(t)
        try:
            df_zscores = get_zscores(df_adjret.index[t].strftime("%Y-%m-%d"), h5_filename)
            df_zscores = df_zscores.dropna(axis = 1, how = 'all')
            df_zscores = df_zscores.dropna(axis = 0, how = 'any')
            df_ret = df_adjret.iloc[t,:][df_zscores.index].T
            df_zscores = df_zscores[~df_ret.isnull()]
            df_ret = df_ret[~df_ret.isnull()]
            
            df_secdum = pd.get_dummies(df_sector.iloc[t])
            df_secdum.rename(columns = {'':'Market'}, inplace = True)
            df_secdum['Market'] = 1
            df_secdum = df_secdum.loc[df_zscores.index]
            df_wgt = df_mktcap.iloc[t,:][df_zscores.index].T
            df_wgt = np.sqrt(df_wgt)
            
            df_x = pd.concat([df_secdum, df_zscores], axis = 1)
            df_y = df_ret - df_ret.mean()
            df_x = df_x.multiply(df_wgt, axis=0)
            df_y = df_y.multiply(df_wgt, axis=0)
            P = np.dot(df_x.values.T, df_x.values).astype(float)
            q = np.dot(df_x.values.T, df_y.values)
            
            e = np.array([0.0] + [1.0]*(df_secdum.shape[1] - 1) + [0.0] * df_zscores.shape[1])
            e[0] = 0.0
            r = quadprog.solve_qp(P, q, np.vstack(e), np.zeros(1, float), 1)[0]
            zf = pd.DataFrame(r, index = df_x.columns, columns = [df_adjret.index[t]]).T
            zsfactors.append(zf)
        except Exception:
            print("Fail") 
    df_zsfactors = pd.concat(zsfactors)
    df_zsfactors.fillna(0, inplace = True)
    df_zsfactors.to_hdf(h5_filename, 'zsfactors')
    return df_zsfactors
    
def write_zifactors(min_cap = 3e8, h5_filename = 'factor.h5'):
    df_adjret = pd.read_hdf(h5_filename, 'adjret')
    df_mktcap = pd.read_hdf(h5_filename, 'mktcap')
    df_industry = pd.read_hdf(h5_filename, 'industry')
    df_adjret[df_mktcap < min_cap] = np.nan
    df_industry[df_mktcap < min_cap] = np.nan
    df_industry[df_industry.isnull()] = ""
    zifactors = []
    for t in range(len(df_adjret)):
        print(t)
        try:
            df_zscores = get_zscores(df_adjret.index[t].strftime("%Y-%m-%d"), h5_filename)
            df_zscores = df_zscores.dropna(axis = 1, how = 'all')
            df_zscores = df_zscores.dropna(axis = 0, how = 'any')
            df_ret = df_adjret.iloc[t,:][df_zscores.index].T
            df_zscores = df_zscores[~df_ret.isnull()]
            df_ret = df_ret[~df_ret.isnull()]
            
            df_inddum = pd.get_dummies(df_industry.iloc[t])
            df_inddum.rename(columns = {'':'Market'}, inplace = True)
            df_inddum['Market'] = 1
            df_inddum = df_inddum.loc[df_zscores.index]
            df_wgt = df_mktcap.iloc[t,:][df_zscores.index].T
            df_wgt = np.sqrt(df_wgt)
            
            df_x = pd.concat([df_inddum, df_zscores], axis = 1)
            df_y = df_ret - df_ret.mean()
            df_x = df_x.multiply(df_wgt, axis=0)
            df_y = df_y.multiply(df_wgt, axis=0)
            P = np.dot(df_x.values.T, df_x.values).astype(float)
            q = np.dot(df_x.values.T, df_y.values)
            
            e = np.array([0.0] + [1.0]*(df_inddum.shape[1]-1) + [0.0] * df_zscores.shape[1])
            e[0] = 0.0
            r = quadprog.solve_qp(P, q, np.vstack(e), np.zeros(1, float), 1)[0]
            zf = pd.DataFrame(r, index = df_x.columns, columns = [df_adjret.index[t]]).T
            zifactors.append(zf)
        except Exception:
            print("Fail")   
    df_zifactors = pd.concat(zifactors)
    df_zifactors.fillna(0, inplace = True)
    df_zifactors.to_hdf(h5_filename, 'zifactors')    
    return df_zifactors

def write_portret(h5_filename = 'factor.h5'):
    """
    Write portfolio returns to the h5 file.
    :param h5_filename: File to store positions to.
    """
    df_portfolio_ret_1 = pd.read_csv('data/back_test_1.csv')
    df_portfolio_ret_1['date'] = [datetime.strptime(d, '%Y-%m-%d') for d in df_portfolio_ret_1.Date]
    df_portfolio_ret_1.set_index('date', inplace = True)
    del df_portfolio_ret_1['Date']
    del df_portfolio_ret_1['equity_curve']
    df_portfolio_ret_2 = pd.read_csv('data/back_test_2.csv')
    df_portfolio_ret_2['date'] = [datetime.strptime(d, '%Y-%m-%d') for d in df_portfolio_ret_2.Date]
    df_portfolio_ret_2.set_index('date', inplace = True)
    del df_portfolio_ret_2['Date']
    del df_portfolio_ret_2['equity_curve']
    df_portfolio_ret = df_portfolio_ret_1.merge(df_portfolio_ret_2, left_index = True, right_index = True, suffixes=('','_'))
    df_portfolio_ret.returns.fillna(df_portfolio_ret.returns_)
    df_portfolio_ret = df_portfolio_ret_1.merge(df_portfolio_ret_2, how = 'outer', left_index = True, right_index = True, suffixes=('','_'))
    df_portfolio_ret.returns.fillna(df_portfolio_ret.returns_, inplace = True)
    del df_portfolio_ret['returns_']
    df_portfolio_ret.to_hdf(h5_filename, 'portret')
    return df_portfolio_ret

def read_pos_file(filename = "data/feb_wgt.csv"):
    """
    Helper for reading portfolio position files.
    """
    df_rawpos = pd.read_csv(filename)
    df_rawpos['date'] = [datetime.strptime(d, '%Y-%m-%d') for d in df_rawpos.TradeDate]
    df_rawpos.set_index('date', inplace = True)
    dates = [d for d in sorted(np.unique(df_rawpos.index))]
    names = sorted(np.unique(df_rawpos.Ticker))
    df_portpos = pd.DataFrame(0, columns = names, index = dates)
    for e in names:
        df_e = df_rawpos[df_rawpos.Ticker == e]
        df_r = pd.DataFrame(df_e.weight.values, index = df_e.TradeDate).reindex(dates)
        df_portpos[e] = df_r
    df_portpos.fillna(0, inplace = True)
    return df_portpos

def write_portpos(h5_filename = 'factor.h5'):
    """
    Write portfolio weights to the h5 file.
    :param h5_filename: File to store positions to.
    """
    df_portpos_1 = read_pos_file("data/aug_sep_wgt.csv")
    df_portpos_2 = read_pos_file("data/feb_wgt.csv")
    df_portpos = pd.concat([df_portpos_1, df_portpos_2])
    df_portpos.fillna(0, inplace = True)
    # Store result to h5
    df_portpos.to_hdf(h5_filename, 'portpos')
    return df_portpos


if __name__ == "__main__":
    write_portpos()